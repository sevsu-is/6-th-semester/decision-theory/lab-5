#include <iostream>

using namespace std;

const int row = 8, column = 5;
int matrix[row][column] = {
        /*              K1     K2     K3      K4     K5  */
        /*x1*/{3, 5, 5, 4, 4},
        /*x2*/{4, 4, 4, 5, 4},
        /*x3*/{5, 4, 3, 3, 5},
        /*x4*/{3, 5, 3, 5, 3},
        /*x5*/{4, 2, 4, 5, 5},
        /*x6*/{3, 5, 3, 5, 3},
        /*x7*/{5, 3, 4, 3, 4},
        /*x8*/{4, 5, 3, 4, 3},
};

int i, j, k;
int n = 0;
int sum[row] = {0, 0, 0, 0, 0, 0, 0, 0},
index[row] = {-1, -1, -1, -1, -1, -1, -1, -1};

// Часть 1 начало
// Матрица предпочтения
int A1[column][column] = {
        {0, 1, 0, 0, 0},
        {0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0}
},
//Матрица эквивалентности
A2[column][column] = {
        {0, 0, 0, 0, 0},
        {0, 0, 1, 0, 0},
        {0, 1, 0, 1, 0},
        {0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0}
};
// Часть 1 конец

int main() {
    setlocale(LC_ALL, "Russian");

    cout << "Исходная матрица:" << endl;
    cout << "       K1 K2 K3 K4 K5" << endl;
    for (i = 0; i < row; i++) {
        cout << "X" << i + 1 << ": {  ";
        for (j = 0; j < column; j++) {
            cout << matrix[i][j] << "  ";
        }
        cout << "}" << endl;
    }
    cout << endl;

    cout << "Матрица предпочтений:" << endl;
    cout << "   K1 K2 K3 K4 K5" << endl;
    for (i = 0; i < column; i++) {
        cout << "K" << i + 1 << " ";
        for (j = 0; j < column; j++) {
            cout << A1[i][j] << "  ";
        }
        cout << endl;
    }
    cout << endl;

    cout << "Матрица эквивалентности:" << endl;
    cout << "   K1 K2 K3 K4 K5" << endl;
    for (i = 0; i < column; i++) {
        cout << "K" << i + 1 << " ";
        for (j = 0; j < column; j++) {
            cout << A2[i][j] << "  ";
        }
        cout << endl;
    }

    /*ОПРЕДЕЛЕНИЕ НЕСРАВНИМЫХ РЕШЕНИЙ*/
    for (i = 0; i < row; i++) {
        for (j = 0; j < column; j++) {
            sum[i] = sum[i] + matrix[i][j];
        }
    }

    for (k = 0; k < row; k++) {
        for (i = 0; i < row; i++) {
            // исключение эквивалентности (сумма равна и элементы равны)
            if (sum[k] == sum[i] && k < i) {
                for (j = 0; j < column; j++) {
                    if (matrix[k][j] == matrix[i][j]) {
                        n++;
                    }
                }
                if (n == column) {
                    for (j = 0; j < column; j++) {
                        matrix[i][j] = 0;
                    }
                }
                n = 0;
            }
            // исключение доминируемого (чистого доминирования)
            if (sum[k] > sum[i] && k < i) {
                for (j = 0; j < column; j++) {
                    if (matrix[k][j] > matrix[i][j] || matrix[k][j] == matrix[i][j]) {
                        n++;
                    }
                }
                if (n == column) {
                    for (j = 0; j < column; j++) {
                        matrix[i][j] = 0;
                    }
                }
                n = 0;
            }
            // исключение доминируемого (чистого доминирования)
            if (sum[k] < sum[i] && k < i) {
                for (j = 0; j < column; j++) {
                    if (matrix[k][j] < matrix[i][j] || matrix[k][j] == matrix[i][j]) {
                        n++;
                    }
                }
                if (n == column) {
                    for (j = 0; j < column; j++) {
                        matrix[k][j] = 0;
                    }
                }
                n = 0;
            }
        }
    }
    /*КОНЕЦ ОПРЕДЕЛЕНИЯ НЕ СРАВНИМЫХ РЕШЕНИЙ*/

    /*ФОРМИРОВАНИЕ ВЕКТОРНЫХ ОЦЕНОК СВЯЗАННЫХ ОТНОШЕНИЕМ ПРЕДПОЧТЕНИЯ*/
    // Для отношения ДОМИНИРОВАНИЯ
    int index_i, index_j;
    int temp, m = 0;
    for (int c = 0; c < column; c++) {
        for (int h = 0; h < column; h++) {
            // там где есть доминирование классов по важности
            if (A1[c][h] == 1) {
                index_i = c;
                index_j = h;
                m++;
                for (k = 0; k < row; k++) {
                    // Поменять местами
                    temp = matrix[k][index_i];
                    matrix[k][index_i] = matrix[k][index_j];
                    matrix[k][index_j] = temp;

                    // попробовать опять исключить доминирование эквивалентность
                    for (i = 0; i < row; i++) {
                        if (sum[k] == sum[i] && k < i) {
                            for (j = 0; j < column; j++) {
                                if (matrix[k][j] == matrix[i][j]) {
                                    n++;
                                }
                            }
                            if (n == column) {
                                for (j = 0; j < column; j++) {
                                    matrix[i][j] = 0;
                                }
                            }
                            n = 0;
                        }
                        if (sum[k] > sum[i] && k < i) {
                            for (j = 0; j < column; j++) {
                                if (matrix[k][j] > matrix[i][j] || matrix[k][j] == matrix[i][j]) {
                                    n++;
                                }
                            }

                            if (n == column) {
                                for (j = 0; j < column; j++) {
                                    matrix[i][j] = 0;
                                }
                            }
                            n = 0;
                        }
                        if (sum[k] < sum[i] && k < i) {
                            for (j = 0; j < column; j++) {
                                if (matrix[k][j] < matrix[i][j] || matrix[k][j] == matrix[i][j]) {
                                    n++;
                                }
                            }
                            if (n == column) {
                                for (j = 0; j < column; j++) {
                                    matrix[k][j] = 0;
                                }
                            }
                            n = 0;
                        }
                    }
                    // Вернуть как было
                    temp = matrix[k][index_j];
                    matrix[k][index_j] = matrix[k][index_i];
                    matrix[k][index_i] = temp;
                }
            }
        }
    }

    // Для отношения ЭКВИВАЛЕНТНОСТИ (поменять местами, убрать экв, убрать домин, вернуть как было)
    m = 0;
    for (int c = 0; c < column; c++) {
        for (int h = 0; h < column; h++) {
            if (A2[c][h] == 1) {
                index_i = c;
                index_j = h;
                m++;
                for (k = 0; k < row; k++) {
                    temp = matrix[k][index_i];
                    matrix[k][index_i] = matrix[k][index_j];
                    matrix[k][index_j] = temp;

                    for (i = 0; i < row; i++) {
                        if (sum[k] == sum[i] && k < i) {
                            for (j = 0; j < column; j++) {
                                if (matrix[k][j] == matrix[i][j]) {
                                    n++;
                                }
                            }
                            if (n == column) {
                                for (j = 0; j < column; j++) {
                                    matrix[i][j] = 0;
                                }
                            }
                            n = 0;
                        }
                        if (sum[k] > sum[i] && k < i) {
                            for (j = 0; j < column; j++) {
                                if (matrix[k][j] > matrix[i][j] || matrix[k][j] == matrix[i][j]) {
                                    n++;
                                }
                            }

                            if (n == column) {
                                for (j = 0; j < column; j++) {
                                    matrix[i][j] = 0;

                                }
                            }
                            n = 0;
                        }
                        if (sum[k] < sum[i] && k < i) {
                            for (j = 0; j < column; j++) {
                                if (matrix[k][j] < matrix[i][j] || matrix[k][j] == matrix[i][j]) {
                                    n++;
                                }
                            }
                            if (n == column) {
                                for (j = 0; j < column; j++) {
                                    matrix[k][j] = 0;
                                }
                            }
                            n = 0;
                        }
                    }
                    temp = matrix[k][index_j];
                    matrix[k][index_j] = matrix[k][index_i];
                    matrix[k][index_i] = temp;
                }
            }
        }
    }

    for (i = 0; i < row; i++) {
        for (j = 0; j < column; j++) {
            if (matrix[i][j] != 0) {
                index[i] = i;
            }

        }
    }

    cout << endl;
    cout << "Множество несравнимых решений с учетом информации о предпочтениях и эквивалентности критериев:" << endl;
    cout << "       K1 K2 K3 K4 K5" << endl;
    for (i = 0; i < row; i++) {
        if (index[i] != -1) {
            cout << "X" << i + 1 << ": {  ";
            for (j = 0; j < column; j++) {
                cout << matrix[i][j] << "  ";
            }
            cout << "}" << endl;
        }
    }
}